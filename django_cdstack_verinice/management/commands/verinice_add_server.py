from django.conf import settings
from django.core.management.base import BaseCommand
from requests import Session
from requests.auth import HTTPBasicAuth  # or HTTPDigestAuth, or OAuth1, etc.
from zeep import Client
from zeep.transports import Transport


class Command(BaseCommand):
    help = "Add server to verinice"

    namespaces = {
        "sync": "http://www.sernet.de/sync/sync",
        "data": "http://www.sernet.de/sync/data",
        "map": "http://www.sernet.de/sync/mapping",
    }

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        proxies = {}

        rsession = Session()
        rsession.auth = HTTPBasicAuth(settings.VERINICE_USER, settings.VERINICE_PASSWD)
        rsession.proxies.update(proxies)

        transport = Transport(session=rsession)

        client = Client(
            "https://itsm.dolphin-it.de/veriniceserver/sync/sync?wsdl",
            transport=transport,
            strict=False,
        )
        # client = Client('http://soapclient.com/xml/SQLDataSoap.WSDL', transport=transport, strict=False)
        # client.options(raw_response=True)

        # client.wsdl.dump()

        # with client.options(raw_response=True):
        #     result = client.service.submitXmlMessage(partnerno=partnerno, xml=xml_pretty_string,
        #                                              signature=xml_signature)

        request_data = {
            "syncRequest": {
                "syncData": {
                    "syncObject": {
                        "extId": "Server4711",
                        "extObjectType": "server",
                    }
                },
                "syncMapping": {"mapObjectType": {}},
            },
        }

        result = client.service.sync(**request_data)
